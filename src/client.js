
const accessToken = new URLSearchParams(document.cookie.replaceAll('; ', '&')).get('access_token')

function apiCall (endpoint, payload, method = 'GET') {
  const opts = {
    headers: {
      Authorization: 'token ' + accessToken,
      accept: 'application/vnd.github.v3+json',
      'Content-Type': 'application/json'
    },
    method: method
  }
  if (payload) {
    if (method === 'GET') { opts.method = 'POST' }
    opts.body = JSON.stringify(payload)
  }
  return fetch('https://api.github.com' + endpoint, opts).then(res => res.json())
}

const gistForm = document.getElementById('gist')

if (accessToken) {
  document.getElementById('welcome').remove()
  gistForm.style.display = 'block'

  apiCall('/gists').then(list => apiCall('/gists/' + list[0].id)).then(gist => {
    document.body.classList.remove('loading')
    console.log({ gist })
    gistForm.querySelector('input[name="gist_id"]').value = gist.id
    gistForm.querySelector('input[name="description"]').value = gist.description
  })
}

gistForm.addEventListener('submit', e => {
  e.preventDefault()
  e.target.classList.add('loading')
  const data = Object.fromEntries(new FormData(e.target))

  apiCall('/gists/' + data.gist_id, data).then(result => {
    console.log(result)
    e.target.classList.remove('loading')
  })
})
