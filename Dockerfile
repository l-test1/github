#
# Билд
#
FROM node:14-alpine3.10 as builder

WORKDIR /sources
COPY . .

RUN npm ci --ignore-scripts
ENV PARCEL_WORKERS=1
RUN npm run build

# удаляем devDependencies
RUN npm prune --production
# Чистка node_modules от "хлама" файлов README LICENSE etc
RUN npx modclean --no-progress --run

# Удалеям более не нужные файлы
RUN rm -rf src package.json package-lock.json


#
# Конечный image
#
FROM mhart/alpine-node:slim-14

WORKDIR /app
COPY --from=builder /sources .

EXPOSE 9000

CMD [ "node", "index.js" ]
