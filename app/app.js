const port = process.env.PORT || 9000

const express = require('express')
const cookieParser = require('cookie-parser')
const fetch = require('node-fetch')

const app = express()
app.use(express.static(__dirname + '/../public'))
app.use(cookieParser())

app.get('/login', (req, res) => {
  res.redirect(`https://github.com/login/oauth/authorize?scope=gist&client_id=${process.env.CLIENT_ID}&redirect_uri=${process.env.REDIRECT_URL}`)
})

app.get('/oauth/callback', async (req, res) => {
  const endpoint = new URL('https://github.com/login/oauth/access_token')
  endpoint.searchParams.append('client_id', process.env.CLIENT_ID)
  endpoint.searchParams.append('client_secret', process.env.CLIENT_SECRET)
  endpoint.searchParams.append('code', req.query.code)

  const response = await fetch(endpoint.toString(), {
    method: 'POST',
    headers: {
      accept: 'application/json'
    }
  }).catch(err => {
    console.error(err)
    res.status(400).send('auth error')
  })
  const { access_token } = await response.json()

  if (!access_token) {
    return res.status(400).send('auth error')
  }

  res.cookie('access_token', access_token, { maxAge: 900000000000000, httpOnly: false })
  res.redirect('/')
})

app.listen(port)
